const { expect } = require('chai');
const { remote } = require('webdriverio');

const options = {
  hostname: 'localhost',
  port: 4444,
  path: '/wd/hub',
  capabilities: {
    browserName: 'firefox',
    browserVersion: '57.0',
    'selenoid:options': {
      enableVNC: true,
      enableVideo: false
    }
  }
};

it('should get homepage', async () => {
    const browser = await remote(options);

    await browser.navigateTo('https://rozetka.com.ua/');
    const input = await browser.$('.header__logo');
    expect(input).not.be.null;
});
